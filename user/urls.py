from django.urls import path
from user import views


urlpatterns = [
    path("user", views.UserView.as_view(), name='user_test'),
    path("alarm_user/<str:id>/", views.AlarmUserON.as_view(), name='alarm_user_on'),
    path("avatar_user", views.AvatarUser.as_view(), name='avatar'),
]