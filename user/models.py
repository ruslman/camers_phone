from django.db import models
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    alarm_on = models.BooleanField(default=False)
    avatar = models.ImageField(upload_to="avatar/")
