from rest_framework.generics import GenericAPIView

from user.serializers import UserGetSerializer, UserPostSerializer, UserAlarmSerializer, UserAvatarSerializer

from rest_framework.response import Response
from user.models import User
import jwt
from camers_phone import settings
from drf_yasg.utils import swagger_auto_schema

# Create your views here.

class UserView(GenericAPIView):
    queryset = ''
    serializer_class = UserPostSerializer

    @swagger_auto_schema(
        responses={
            200: UserGetSerializer,
        },
    )
    def get(self, request):
        token = request.META.get('HTTP_AUTHORIZATION', " ")
        result = check_permition(token)
        if not result:
            return Response({"message": "You do not have permitions"})
            pass
        payload_decoded = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        queryset = User.objects.filter(id=payload_decoded['user_id'])
        serializer = UserGetSerializer(queryset, many=True)
        return Response(serializer.data)

    def post(self, request):
        token = request.META.get('HTTP_AUTHORIZATION', " ")
        result = check_permition(token)
        if not result:
            return Response({"message": "You do not have permitions"})
            pass
        payload_decoded = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        a = User.objects.filter(id=payload_decoded['user_id']).first()
        a.username = request.data['username']
        a.email = request.data['email']
        a.save()
        return Response(201)


class AlarmUserON(GenericAPIView):
    queryset = ''
    serializer_class = UserAlarmSerializer

    def get(self, request, id):
        token = request.META.get('HTTP_AUTHORIZATION', " ")
        result = check_permition(token)
        if not result:
            return Response({"message": "You do not have permitions"})
            pass
        queryset = User.objects.filter(id=id)
        serializer = UserAlarmSerializer(queryset, many=True)
        return Response(serializer.data)

    def post(self, request, id):
        token = request.META.get('HTTP_AUTHORIZATION', " ")
        result = check_permition(token)
        if not result:
            return Response({"message": "You do not have permitions"})
            pass
        User.objects.filter(id=id).update(alarm_on=request.data['alarm_on'])
        if not request.data['alarm_on']:
            Device.objects.filter(owner_id=id).update(is_active=False)
        return Response(201)

class AvatarUser(GenericAPIView):
    queryset = ''
    serializer_class = UserAvatarSerializer

    def get(self, request):
        token = request.META.get('HTTP_AUTHORIZATION', " ")
        result = check_permition(token)
        if not result:
            return Response({"message": "You do not have permitions"})
            pass
        payload_decoded = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        queryset = User.objects.filter(id=payload_decoded['user_id'])
        serializer = UserAvatarSerializer(queryset, many=True)
        return Response(serializer.data)

    def post(self, request):
        token = request.META.get('HTTP_AUTHORIZATION', " ")
        result = check_permition(token)
        if not result:
            return Response({"message": "You do not have permitions"})
            pass
        payload_decoded = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        a = User.objects.filter(id=payload_decoded['user_id']).first()
        a.avatar = request.FILES['avatar']
        a.save()
        return Response(201)

def check_permition(token):
    try:
        jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        result = True
    except:
        result = False
    return result
