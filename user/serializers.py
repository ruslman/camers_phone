from rest_framework import serializers

from .models import User

class UserGetSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = "__all__"

class UserPostSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ["username", "email"]

class UserAlarmSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ["alarm_on"]

class UserAvatarSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ["avatar"]