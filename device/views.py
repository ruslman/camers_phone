from rest_framework.generics import GenericAPIView

from device.models import Device, Alarm, AlarmType
from device.serializers import DeviceGetSerializer, DevicePostSerializer, AlarmGetSerializer, AlarmPostSerializer, DeviceDeleteSerializer, AlarmDeleteSerializer, CameraActiveSerializer, AlarmPostDetailSerializer

from rest_framework.response import Response
from user.models import User
import jwt
from camers_phone import settings
from drf_yasg.utils import swagger_auto_schema
from django.views.decorators.csrf import csrf_exempt

class DeviceView(GenericAPIView):
    queryset = ''
    serializer_class = DeviceGetSerializer

    def get(self, request):
        token = request.META.get('HTTP_AUTHORIZATION', " ")
        result = check_permition(token)
        if not result:
            return Response({"message": "You do not have permitions"})

        payload_decoded = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        queryset = Device.objects.filter(owner_id=payload_decoded['user_id'])
        serializer = DeviceGetSerializer(queryset, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(
        request_body=DevicePostSerializer,
    )
    def post(self, request):
        token = request.META.get('HTTP_AUTHORIZATION', " ")
        result = check_permition(token)
        if not result:
            return Response({"message": "You do not have permitions"})
            pass
        payload_decoded = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        try:
            a = Device.objects.create(name=request.data['name'], owner_id=payload_decoded['user_id'], is_camera=request.data['is_camera'])
        except:
            return Response(500)
        serializer = DeviceGetSerializer(a)
        return Response(serializer.data)


class AlarmView(GenericAPIView):
    queryset = ''
    serializer_class = AlarmGetSerializer

    def get(self, request):
        token = request.META.get('HTTP_AUTHORIZATION', " ")
        result = check_permition(token)
        if not result:
            return Response({"message": "You do not have permitions"})
        payload_decoded = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        queryset = Alarm.objects.filter(device__owner_id=payload_decoded['user_id']).order_by('-date_alarm')
        serializer = AlarmGetSerializer(queryset, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(
        request_body=AlarmPostSerializer,
    )
    def post(self, request):
        token = request.META.get('HTTP_AUTHORIZATION', " ")
        result = check_permition(token)
        if not result:
            return Response({"message": "You do not have permitions"})
            pass
        payload_decoded = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        if Device.objects.get(id=request.data['device']).owner.id != payload_decoded['user_id']:
            return Response({"message": "Устройство вам не пренадлежит"})
        if not Device.objects.get(id=request.data['device']).is_active:
            return Response({"message": "Датчик не живой"})
        a = Alarm(device_id=request.data['device'], type_alarm_id=request.data['type_alarm'], file=request.data['file'])
        a.save()
        serializer = AlarmGetSerializer(a)
        return Response(serializer.data)

class DetailDevice(GenericAPIView):
    queryset = ''
    serializer_class = DeviceGetSerializer

    def get(self, request, id):
        token = request.META.get('HTTP_AUTHORIZATION', " ")
        result = check_permition(token)
        if not result:
            return Response({"message": "You do not have permitions"})
            pass
        payload_decoded = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        queryset = Device.objects.filter(id=id, owner_id=payload_decoded['user_id'])
        serializer = DeviceGetSerializer(queryset, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(
        request_body=DevicePostSerializer,
    )
    def post(self, request, id):
        token = request.META.get('HTTP_AUTHORIZATION', " ")
        result = check_permition(token)
        if not result:
            return Response({"message": "You do not have permitions"})
            pass
        payload_decoded = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        try:
            Device.objects.filter(id=id, owner_id=payload_decoded['user_id']).update(name=request.data['name'], is_camera=request.data['is_camera'])
        except:
            return Response(500)
        return Response(201)

    @swagger_auto_schema(
        request_body=DeviceDeleteSerializer,
    )
    def delete(self, request, id):
        token = request.META.get('HTTP_AUTHORIZATION', " ")
        result = check_permition(token)
        if not result:
            return Response({"message": "You do not have permitions"})
            pass
        payload_decoded = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        try:
            Device.objects.filter(id=id, owner_id=payload_decoded['user_id']).delete()
        except:
            return Response(500)
        return Response(201)

class DetailAlarm(GenericAPIView):
    queryset = ''
    serializer_class = AlarmGetSerializer

    def get(self, request, id):
        token = request.META.get('HTTP_AUTHORIZATION', " ")
        result = check_permition(token)
        if not result:
            return Response({"message": "You do not have permitions"})
            pass
        payload_decoded = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        queryset = Alarm.objects.filter(id=id, device__owner_id=payload_decoded['user_id'])
        serializer = AlarmGetSerializer(queryset, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(
        request_body=AlarmPostDetailSerializer,
    )
    def post(self, request, id):
        Alarm.objects.filter(id=id).update(is_watched=request.data['is_watched'])
        return Response(201)

    @swagger_auto_schema(
        request_body=AlarmDeleteSerializer,
    )
    def delete(self, request, id):
        token = request.META.get('HTTP_AUTHORIZATION', " ")
        result = check_permition(token)
        if not result:
            return Response({"message": "You do not have permitions"})
            pass
        payload_decoded = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        try:
            Alarm.objects.filter(id=id, device__owner_id=payload_decoded['user_id']).delete()
        except:
            return Response(500)
        return Response(201)

class CameraActive(GenericAPIView):
    queryset = ''
    serializer_class = CameraActiveSerializer

    def get(self, request, id):
        token = request.META.get('HTTP_AUTHORIZATION', " ")
        result = check_permition(token)
        if not result:
            return Response({"message": "You do not have permitions"})
            pass
        queryset = Device.objects.filter(id=id)
        serializer = CameraActiveSerializer(queryset, many=True)
        return Response(serializer.data)

    def post(self, request, id):
        Device.objects.filter(id=id).update(is_active=request.data['is_active'])
        return Response(201)



def check_permition(token):
    try:
        jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        result = True
    except:
        result = False
    return result
