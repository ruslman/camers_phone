from rest_framework import serializers

from .models import Device, Alarm, AlarmType

class DeviceGetSerializer(serializers.ModelSerializer):

    class Meta:
        model = Device
        fields = "__all__"

class DevicePostSerializer(serializers.ModelSerializer):

    class Meta:
        model = Device
        fields = ["name", "is_camera"]

class DeviceDeleteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Device
        fields = []


class AlarmGetSerializer(serializers.ModelSerializer):

    class Meta:
        model = Alarm
        fields = "__all__"

class AlarmPostSerializer(serializers.ModelSerializer):

    class Meta:
        model = Alarm
        fields = ["device", "type_alarm", "file"]

class AlarmDeleteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Alarm
        fields = []

class CameraActiveSerializer(serializers.ModelSerializer):

    class Meta:
        model = Device
        fields = ["is_active"]

class AlarmPostDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Alarm
        fields = ["is_watched"]
