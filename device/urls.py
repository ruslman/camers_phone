from django.urls import path
from device import views


urlpatterns = [
    path("device/", views.DeviceView.as_view(), name='device_test'),
    path("alarm/", views.AlarmView.as_view(), name='alarm_test'),
    path("device/<str:id>/", views.DetailDevice.as_view(), name='detail_device_test'),
    path("alarm/<str:id>/", views.DetailAlarm.as_view(), name='detail_alarm_test'),
    path("camera_active/<str:id>/", views.CameraActive.as_view(), name='camera_active')
]