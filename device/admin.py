from django.contrib import admin
from .models import AlarmType, Alarm

# Register your models here.
admin.site.register(AlarmType, admin.ModelAdmin)

admin.site.register(Alarm, admin.ModelAdmin)