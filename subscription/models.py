from django.db import models
from django.conf import settings

class Subscription(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название')
    price = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='Цена')

class Order(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name='Пользователь', related_name='order_user')
    subscription = models.ForeignKey('Subscription', on_delete=models.CASCADE, verbose_name='Подписка')
    date_order = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')